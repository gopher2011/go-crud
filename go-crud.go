package main

import (
	"flag"
	"fmt"
	"gitee.com/gopher2011/go-crud/internal/config"
	"gitee.com/gopher2011/go-crud/internal/handler"
	"gitee.com/gopher2011/go-crud/internal/svc"
	"github.com/gin-gonic/gin"

	"github.com/zeromicro/go-zero/core/conf"
)

var configFile = flag.String("f", "etc/go-crud-api.yaml", "the config file")

func main() {
	flag.Parse()
	var c config.Config
	conf.MustLoad(*configFile, &c)
	svc.NewServiceContext(c)
	svc.Svc.AutoMigrate()
	r := gin.Default()
	handler.RegisterHandlers(r)
	r.Run(fmt.Sprintf(":%d", c.Port))
}
