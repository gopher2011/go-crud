package user

import (
	"context"
	"gitee.com/gopher2011/go-crud/internal/svc"
	"github.com/zeromicro/go-zero/core/logx"
)

type User struct {
	logx.Logger
	Ctx    context.Context
	SvcCtx *svc.ServiceContext
}

func NewUser(ctx context.Context, svcCtx *svc.ServiceContext) *User {
	return &User{
		Logger: logx.WithContext(ctx),
		Ctx:    ctx,
		SvcCtx: svcCtx,
	}
}
