package user

import (
	"gitee.com/gopher2011/go-crud/internal/types"

	"github.com/gogf/gf/util/gvalid"
	"github.com/gogf/gf/v2/util/gconv"
)

func (l *User) CreateUser(req types.BaseReq) (*types.Response, error) {
	if err := gvalid.CheckStruct(l.Ctx, &req, nil); err != nil {
		return nil, err
	}
	var arg types.SqlParams
	if err := gconv.Struct(req, &arg); err != nil {
		return nil, err
	}
	// 这里开始写业务代码
	return &types.Response{}, nil
}
