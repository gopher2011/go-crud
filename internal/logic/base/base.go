package base

import (
	"context"
	"gitee.com/gopher2011/go-crud/internal/svc"
	"github.com/zeromicro/go-zero/core/logx"
)

type Base struct {
	logx.Logger
	Ctx    context.Context
	SvcCtx *svc.ServiceContext
}

func NewBase(ctx context.Context, svcCtx *svc.ServiceContext) *Base {
	return &Base{
		Logger: logx.WithContext(ctx),
		Ctx:    ctx,
		SvcCtx: svcCtx,
	}
}
