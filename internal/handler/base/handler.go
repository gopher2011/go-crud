package handler

import (
	"gitee.com/gopher2011/go-crud/internal/logic/base"
	"gitee.com/gopher2011/go-crud/internal/svc"
	"gitee.com/gopher2011/go-crud/internal/types"
	"github.com/gin-gonic/gin"
)

func Login(c *gin.Context) {
	var req types.LoginName
	if err := c.ShouldBind(&req); err != nil {
		c.JSON(200, gin.H{"code": 400, "msg": err.Error(), "data": nil})
		return
	}
	l := base.NewBase(c.Request.Context(), svc.Svc)
	resp, err := l.Login(req)
	if err != nil {
		c.JSON(200, gin.H{"code": 400, "msg": err.Error(), "data": nil})
	} else {
		c.JSON(200, resp)
	}
}

func Register(c *gin.Context) {
	var req types.LoginReq
	if err := c.ShouldBind(&req); err != nil {
		c.JSON(200, gin.H{"code": 400, "msg": err.Error(), "data": nil})
		return
	}
	l := base.NewBase(c.Request.Context(), svc.Svc)
	err := l.Register(req)
	if err != nil {
		c.JSON(200, gin.H{"code": 400, "msg": err.Error(), "data": nil})
	} else {
		c.JSON(200, nil)
	}
}
