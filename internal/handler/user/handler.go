package handler

import (
	"gitee.com/gopher2011/go-crud/internal/logic/user"
	"gitee.com/gopher2011/go-crud/internal/svc"
	"gitee.com/gopher2011/go-crud/internal/types"
	"github.com/gin-gonic/gin"
)

func GetUser(c *gin.Context) {
	var req types.BaseReq
	if err := c.ShouldBind(&req); err != nil {
		c.JSON(200, gin.H{"code": 400, "msg": err.Error(), "data": nil})
		return
	}
	l := user.NewUser(c.Request.Context(), svc.Svc)
	resp, err := l.GetUser(req)
	if err != nil {
		c.JSON(200, gin.H{"code": 400, "msg": err.Error(), "data": nil})
	} else {
		c.JSON(200, resp)
	}
}

func CreateUser(c *gin.Context) {
	var req types.BaseReq
	if err := c.ShouldBind(&req); err != nil {
		c.JSON(200, gin.H{"code": 400, "msg": err.Error(), "data": nil})
		return
	}
	l := user.NewUser(c.Request.Context(), svc.Svc)
	resp, err := l.CreateUser(req)
	if err != nil {
		c.JSON(200, gin.H{"code": 400, "msg": err.Error(), "data": nil})
	} else {
		c.JSON(200, resp)
	}
}
