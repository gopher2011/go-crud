package config

import (
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/rest"
)

type Config struct {
	rest.RestConf
	Database struct {
		Pkg             string
		LogPath         string
		DriverName      string
		Host            string
		Port            int
		User            string
		Password        string
		DBName          string
		MaxOpenConn     int
		MaxIdleConn     int
		ConnMaxLifetime int
		ConnMaxIdleTime int
	}
	CacheRedis cache.ClusterConf
}
