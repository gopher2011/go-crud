// Code generated by goctl. DO NOT EDIT.
package types

type Request struct {
	RequestId string `json:"requestId"`
}

type SqlParams struct {
}

type LoginReq struct {
	Account  *string `json:"account"`
	Password *string `json:"password"`
}

type LoginName struct {
	Name string `path:"name"`
	Age  int    `json:"age"`
}

type BaseReq struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"message"`
	Data interface{} `json:"data"`
}

type User struct {
	Age int `json:"age" gorm:"age" v:"age"`
}
